"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mockAPI_1 = require("./mockAPI");
const entities_1 = require("./entities");
const app = (0, express_1.default)();
const port = 3000;
const apiVersion = '/api/v1';
app.get('/', (req, res) => {
    res.sendStatus(404);
});
app.get(`${apiVersion}/translates`, (req, res) => {
    const lang = req.query.lang
        ? req.query.lang
        : entities_1.LangType.RU;
    const translates = (0, mockAPI_1.getTranslates)(lang);
    res.json(translates);
});
app.get(`${apiVersion}/riddles`, (req, res) => {
    const lang = req.query.lang
        ? req.query.lang
        : entities_1.LangType.RU;
    const riddles = (0, mockAPI_1.getRiddles)(lang);
    res.json(riddles);
});
app.get(`${apiVersion}/letters`, (req, res) => {
    const lang = req.query.lang
        ? req.query.lang
        : entities_1.LangType.RU;
    const letters = (0, mockAPI_1.getLetters)(lang);
    res.json(letters);
});
app.listen(port, () => {
    console.log(`Server listening om port ${port}`);
});
