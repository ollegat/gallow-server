import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { createServer } from '../../src/createServer';
import config, { HTTP_STATUSES } from '../../src/config/config';
import { RiddleModel } from '../../src/app/models/riddles';
import { LangType, RiddleData, ResponseCheckLetter } from '../../src/entities';

const { apiV1 } = config;
const app = createServer();

const mockData = {
    _id: new mongoose.Types.ObjectId(),
    riddle: 'testRiddle',
    answer: 'gtestAnswer',
    lang: LangType.RU,
}

describe('/riddles', () => {

    beforeAll(async () => {
        const mongoServer = await MongoMemoryServer.create();

        await mongoose.connect(mongoServer.getUri());

    });

    afterAll(async () => {
        await mongoose.disconnect();
        await mongoose.connection.close();
    });

    it('should return no content when db has no documents', async () => {
        await request(app)
            .get(`${apiV1}/riddle/data`)
            .expect(HTTP_STATUSES.NO_CONTENT_204);
    });

    it('should return one document riddle data from DB', async () => {
        const riddle = new RiddleModel(mockData);
        riddle.save();

        const createResponse = await request(app)
            .get(`${apiV1}/riddle/data`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createRiddleData = createResponse.body;
        const riddleData: RiddleData = {
            id: mockData._id.toString(),
            riddle: mockData.riddle,
            countLetters: mockData.answer.length+1,
        }
        expect(createRiddleData).toMatchObject(riddleData);
    });

    it('should return true on checkLetter test one', async () => {
        const id = mockData._id.toString();
        const positions = [0];
        const letter = mockData.answer[positions[0]];

        const createResponse = await request(app)
            .get(`${apiV1}/riddles/${id}/checkletter/${encodeURIComponent(letter)}?lang=${LangType.RU}`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createCheckLetter = createResponse.body;
        const result: ResponseCheckLetter = {
            letter,
            status: true,
            positions,
        }
        expect(createCheckLetter).toMatchObject(result);
    });

    it('should return true on checkLetter test two', async () => {
        const id = mockData._id.toString();
        const positions = [1,4];
        const letter = mockData.answer[positions[0]];

        const createResponse = await request(app)
            .get(`${apiV1}/riddles/${id}/checkletter/${encodeURIComponent(letter)}?lang=${LangType.RU}`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createCheckLetter = createResponse.body;
        const result: ResponseCheckLetter = {
            letter,
            status: true,
            positions,
        }
        expect(createCheckLetter).toMatchObject(result);
    });

    it('should return false on checkLetter', async () => {
        const id = mockData._id.toString();
        const letter = 'b';

        const createResponse = await request(app)
            .get(`${apiV1}/riddles/${id}/checkletter/${encodeURIComponent(letter)}?lang=${LangType.RU}`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createCheckLetter = createResponse.body;
        const result: ResponseCheckLetter = {
            letter,
            status: false,
            positions: [],
        }        
        expect(createCheckLetter).toMatchObject(result);
    });

    it('should return 404 id is not valid on checkLetter', async () => {
        const id = 'j';
        const letter = 's';

        const createResponse = await request(app)
            .get(`${apiV1}/riddles/${id}/checkletter/${encodeURIComponent(letter)}?lang=${LangType.RU}`)
            .expect(HTTP_STATUSES.BAD_REQUEST_400);
    });

    it('should return riddle data from DB with two documents', async () => {

        const mockData2 = {
            _id: new mongoose.Types.ObjectId(),
            riddle: 'testRiddle2',
            answer: 'gtestAnswer2',
            lang: LangType.RU,
        }

        const riddle2 = new RiddleModel(mockData2);
        riddle2.save();

        const createResponse = await request(app)
            .get(`${apiV1}/riddle/data`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createRiddleData = createResponse.body;
        const riddleData1: RiddleData = {
            id: mockData._id.toString(),
            riddle: mockData.riddle,
            countLetters: mockData.answer.length+1,
        }

        const riddleData2: RiddleData = {
            id: mockData2._id.toString(),
            riddle: mockData2.riddle,
            countLetters: mockData2.answer.length+1,
        }

        expect([riddleData1.id, riddleData2.id]).toContain(createRiddleData.id);
    });
});
