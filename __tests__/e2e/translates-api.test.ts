import request from 'supertest';
import { createServer } from '../../src/createServer';
import config, { HTTP_STATUSES } from '../../src/config/config';
import { getTranslates } from '../../src/mockAPI';
import { LangType } from '../../src/entities';

const { apiV1 } = config;
const app = createServer();

describe('/translates', () => {

    it('should return russian translates', async () => {
        const createResponse = await request(app)
            .get(`${apiV1}/translates`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createTranslates = createResponse.body;
        const translates = getTranslates(LangType.RU);
        expect(createTranslates).toMatchObject(translates);
    });

    it('should return english translates', async () => {
        const createResponse = await request(app)
            .get(`${apiV1}/translates?lang=${LangType.EN}`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createTranslates = createResponse.body;
        const translates = getTranslates(LangType.EN);
        expect(createTranslates).toMatchObject(translates);
    });

    it('should return Ukrain translates', async () => {
        const createResponse = await request(app)
            .get(`${apiV1}/translates?lang=${LangType.UK}`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createTranslates = createResponse.body;
        const translates = getTranslates(LangType.UK);
        expect(createTranslates).toMatchObject(translates);
    });

});


