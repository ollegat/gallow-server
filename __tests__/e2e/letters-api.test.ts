import request from 'supertest';
import { createServer } from '../../src/createServer';
import config, { HTTP_STATUSES } from '../../src/config/config';
import { getLetters } from '../../src/mockAPI';
import { LangType } from '../../src/entities';

const { apiV1 } = config;
const app = createServer();

describe('/letters', () => {

    it('should return ru letters', async () => {
        const createResponse = await request(app)
            .get(`${apiV1}/letters`)
            .expect(HTTP_STATUSES.OK_200);
        
        const createLetters = createResponse.body;
        const letters = getLetters(LangType.RU);

        expect(createLetters).toMatchObject(letters);
    });

});
