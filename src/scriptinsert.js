

db.riddles.insertMany(
    [
        {
            riddle : 'Fruit',
            answer : 'apple',
            lang: 'EN'
        },
        {
            riddle : 'Cut',
            answer : 'scissors',
            lang: 'EN'
        },
        {
            'riddle' : 'Vegetable',
            'answer' : 'carrot',
            lang: 'EN'
        },
        {
            'riddle' : 'Vegetable',
            'answer' : 'onion',
            lang: 'EN'
        },
        {
            'riddle' : 'Shoes',
            'answer' : 'slippers',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'elephant',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'deer',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'crocodile',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'squirrel',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'wolf',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'tiger',
            lang: 'EN'
        },
        {
            'riddle' : 'Animal',
            'answer' : 'bear',
            lang: 'EN'
        },
        {
            'riddle' : 'Music',
            'answer' : 'guitar',
            lang: 'EN'
        },
        {
            'riddle' : 'Capacity',
            'answer' : 'bottle',
            lang: 'EN'
        },
        {
            'riddle' : 'Night',
            'answer' : 'moon',
            lang: 'EN'
        },
        {
            'riddle' : 'Technique',
            'answer' : 'computer',
            lang: 'EN'
        },
        {
            'riddle' : 'Technique',
            'answer' : 'television',
            lang: 'EN'
        },
        {
            'riddle' : 'Furniture',
            'answer' : 'table',
            lang: 'EN'
        },
        {
            'riddle' : 'Furniture',
            'answer' : 'chair',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'mother',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'dad',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'brother',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'sister',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'son',
            lang: 'EN'
        },
        {
            'riddle' : 'Relative',
            'answer' : 'daughter',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'ship',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'automobile',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'airplane',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'truck',
            lang: 'EN'
        },
        {
            'riddle' : 'Construction',
            'answer' : 'excavator',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'tractor',
            lang: 'EN'
        },
        {
            'riddle' : 'Transport',
            'answer' : 'bus',
            lang: 'EN'
        },
        {
            'riddle' : 'light',
            'answer' : 'electricity',
            lang: 'EN'
        },
        {
            'riddle' : 'Фрукт',
            'answer' : 'яблуко',
            lang: 'UK'
        },
        {
            'riddle' : 'Розрізати',
            'answer' : 'ножиці',
            lang: 'UK'
        },
        {
            'riddle' : 'Черпати',
            'answer' : 'ковшик',
            lang: 'UK'
        },
        {
            'riddle' : 'Овоч',
            'answer' : 'морква',
            lang: 'UK'
        },
        {
            'riddle' : 'Овоч',
            'answer' : 'цибуля',
            lang: 'UK'
        },
        {
            'riddle' : 'Взуття',
            'answer' : 'капці',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'слон',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'олень',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'крокодил',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'білка',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'вовк',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'тигр',
            lang: 'UK'
        },
        {
            'riddle' : 'тварина',
            'answer' : 'ведмідь',
            lang: 'UK'
        },
        {
            'riddle' : 'Музика',
            'answer' : 'гітара',
            lang: 'UK'
        },
        {
            'riddle' : 'Місткість',
            'answer' : 'пляшка',
            lang: 'UK'
        },
        {
            'riddle' : 'Ніч',
            'answer' : 'місяць',
            lang: 'UK'
        },
        {
            'riddle' : 'Техніка',
            'answer' : 'компютер',
            lang: 'UK'
        },
        {
            'riddle' : 'Техніка',
            'answer' : 'телевізор',
            lang: 'UK'
        },
        {
            'riddle' : 'Меблі',
            'answer' : 'стіл',
            lang: 'UK'
        },
        {
            'riddle' : 'Меблі',
            'answer' : 'стілець',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'мати',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'папа',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'брат',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'сестра',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'син',
            lang: 'UK'
        },
        {
            'riddle' : 'Родич',
            'answer' : 'дочка',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'корабель',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'автомобіль',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'літак',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'вантажівка',
            lang: 'UK'
        },
        {
            'riddle' : 'Будівництво',
            'answer' : 'екскаватор',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'трактор',
            lang: 'UK'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'автобус',
            lang: 'UK'
        },
        {
            'riddle' : 'світло',
            'answer' : 'електрика',
            lang: 'UK'
        },
        {
            'riddle' : 'Фрукт',
            'answer' : 'яблоко',
            lang: 'RU'
        },
        {
            'riddle' : 'Разрезать',
            'answer' : 'ножницы',
            lang: 'RU'
        },
        {
            'riddle' : 'Черпать',
            'answer' : 'ковшик',
            lang: 'RU'
        },
        {
            'riddle' : 'Овощ',
            'answer' : 'морковь',
            lang: 'RU'
        },
        {
            'riddle' : 'Овощ',
            'answer' : 'лук',
            lang: 'RU'
        },
        {
            'riddle' : 'Обувь',
            'answer' : 'тапочки',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'слон',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'олень',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'крокодил',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'белка',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'волк',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'тигр',
            lang: 'RU'
        },
        {
            'riddle' : 'Животное',
            'answer' : 'медведь',
            lang: 'RU'
        },
        {
            'riddle' : 'Музыка',
            'answer' : 'гитара',
            lang: 'RU'
        },
        {
            'riddle' : 'Емкость',
            'answer' : 'бутылка',
            lang: 'RU'
        },
        {
            'riddle' : 'Ночь',
            'answer' : 'луна',
            lang: 'RU'
        },
        {
            'riddle' : 'Техника',
            'answer' : 'компьютер',
            lang: 'RU'
        },
        {
            'riddle' : 'Техника',
            'answer' : 'телевизор',
            lang: 'RU'
        },
        {
            'riddle' : 'Мебель',
            'answer' : 'стол',
            lang: 'RU'
        },
        {
            'riddle' : 'Мебель',
            'answer' : 'стул',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'мама',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'папа',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'брат',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'сестра',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'сын',
            lang: 'RU'
        },
        {
            'riddle' : 'Родственник',
            'answer' : 'дочь',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'корабль',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'автомобиль',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'самолет',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'грузовик',
            lang: 'RU'
        },
        {
            'riddle' : 'Строительство',
            'answer' : 'экскаватор',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'трактор',
            lang: 'RU'
        },
        {
            'riddle' : 'Транспорт',
            'answer' : 'автобус',
            lang: 'RU'
        },
        {
            'riddle' : 'свет',
            'answer' : 'электричество',
            lang: 'RU'
        }
    ]
)