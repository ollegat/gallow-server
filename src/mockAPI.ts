import { Riddle, LangType, ResponseLettersType } from './entities';

const ruLetters = ['а','б','в','г','д','е','ж',
'з','и','й','к','л','м','н',
'о','п','р','с','т','у','ф',
'х','ц','ч','ш','щ','ъ','ы',
'ь','э','ю','я'];

const ruLettersKeyboard = [
    ['й','ц','у','к','е','н','г','ш','щ','з','х','ъ'],
    ['ф','ы','в','а','п','р','о','л','д','ж','э'],
    ['я','ч','с','м','и','т','ь','б','ю'],
];

const ukLetters = ['а','б','в','г','ґ','д','е',
'є','ж','з','и','і','ї','й',
'к','л','м','н','о','п','р',
'с','т','у','ф','х','ц','ч',
'ш','щ','ь','ю','я'];

const ukLettersKeyboard = [
    ['й','ц','у','к','е','н','г','ш','щ','з','х','ї'],
    ['ф','і','в','а','п','р','о','л','д','ж','є'],
    ['ґ','я','ч','с','м','и','т','ь','б','ю'],
];

const enLetters = ['a','b','c','d','e','f','g',
'h','i','j','k','l','m','n',
'o','p','q','r','s','t','u',
'v','w','x','y','z'];

const enLettersKeyboard = [
    ['q','w','e','r','t','y','u','i','o','p'],
    ['a','s','d','f','g','h','j','k','l'],
    ['z','x','c','v','b','n','m'],
];

const enRiddles = [
    {
        'riddle' : 'Fruit',
        'answer' : 'apple'
    },
    {
        'riddle' : 'Cut',
        'answer' : 'scissors'
    },
    {
        'riddle' : 'Vegetable',
        'answer' : 'carrot'
    },
    {
        'riddle' : 'Vegetable',
        'answer' : 'onion'
    },
    {
        'riddle' : 'Shoes',
        'answer' : 'slippers'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'elephant'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'deer'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'crocodile'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'squirrel'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'wolf'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'tiger'
    },
    {
        'riddle' : 'Animal',
        'answer' : 'bear'
    },
    {
        'riddle' : 'Music',
        'answer' : 'guitar'
    },
    {
        'riddle' : 'Capacity',
        'answer' : 'bottle'
    },
    {
        'riddle' : 'Night',
        'answer' : 'moon'
    },
    {
        'riddle' : 'Technique',
        'answer' : 'computer'
    },
    {
        'riddle' : 'Technique',
        'answer' : 'television'
    },
    {
        'riddle' : 'Furniture',
        'answer' : 'table'
    },
    {
        'riddle' : 'Furniture',
        'answer' : 'chair'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'mother'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'dad'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'brother'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'sister'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'son'
    },
    {
        'riddle' : 'Relative',
        'answer' : 'daughter'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'ship'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'automobile'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'airplane'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'truck'
    },
    {
        'riddle' : 'Construction',
        'answer' : 'excavator'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'tractor'
    },
    {
        'riddle' : 'Transport',
        'answer' : 'bus'
    },
    {
        'riddle' : 'light',
        'answer' : 'electricity'
    }    
];

const ukRiddles = [
    {
        'riddle' : 'Фрукт',
        'answer' : 'яблуко'
    },
    {
        'riddle' : 'Розрізати',
        'answer' : 'ножиці'
    },
    {
        'riddle' : 'Черпати',
        'answer' : 'ковшик'
    },
    {
        'riddle' : 'Овоч',
        'answer' : 'морква'
    },
    {
        'riddle' : 'Овоч',
        'answer' : 'цибуля'
    },
    {
        'riddle' : 'Взуття',
        'answer' : 'капці'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'слон'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'олень'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'крокодил'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'білка'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'вовк'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'тигр'
    },
    {
        'riddle' : 'тварина',
        'answer' : 'ведмідь'
    },
    {
        'riddle' : 'Музика',
        'answer' : 'гітара'
    },
    {
        'riddle' : 'Місткість',
        'answer' : 'пляшка'
    },
    {
        'riddle' : 'Ніч',
        'answer' : 'місяць'
    },
    {
        'riddle' : 'Техніка',
        'answer' : 'компютер'
    },
    {
        'riddle' : 'Техніка',
        'answer' : 'телевізор'
    },
    {
        'riddle' : 'Меблі',
        'answer' : 'стіл'
    },
    {
        'riddle' : 'Меблі',
        'answer' : 'стілець'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'мати'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'папа'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'брат'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'сестра'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'син'
    },
    {
        'riddle' : 'Родич',
        'answer' : 'дочка'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'корабель'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'автомобіль'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'літак'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'вантажівка'
    },
    {
        'riddle' : 'Будівництво',
        'answer' : 'екскаватор'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'трактор'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'автобус'
    },
    {
        'riddle' : 'світло',
        'answer' : 'електрика'
    }        
];

const ruRiddles = [
    {
        'riddle' : 'Фрукт',
        'answer' : 'яблоко'
    },
    {
        'riddle' : 'Разрезать',
        'answer' : 'ножницы'
    },
    {
        'riddle' : 'Черпать',
        'answer' : 'ковшик'
    },
    {
        'riddle' : 'Овощ',
        'answer' : 'морковь'
    },
    {
        'riddle' : 'Овощ',
        'answer' : 'лук'
    },
    {
        'riddle' : 'Обувь',
        'answer' : 'тапочки'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'слон'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'олень'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'крокодил'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'белка'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'волк'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'тигр'
    },
    {
        'riddle' : 'Животное',
        'answer' : 'медведь'
    },
    {
        'riddle' : 'Музыка',
        'answer' : 'гитара'
    },
    {
        'riddle' : 'Емкость',
        'answer' : 'бутылка'
    },
    {
        'riddle' : 'Ночь',
        'answer' : 'луна'
    },
    {
        'riddle' : 'Техника',
        'answer' : 'компьютер'
    },
    {
        'riddle' : 'Техника',
        'answer' : 'телевизор'
    },
    {
        'riddle' : 'Мебель',
        'answer' : 'стол'
    },
    {
        'riddle' : 'Мебель',
        'answer' : 'стул'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'мама'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'папа'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'брат'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'сестра'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'сын'
    },
    {
        'riddle' : 'Родственник',
        'answer' : 'дочь'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'корабль'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'автомобиль'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'самолет'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'грузовик'
    },
    {
        'riddle' : 'Строительство',
        'answer' : 'экскаватор'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'трактор'
    },
    {
        'riddle' : 'Транспорт',
        'answer' : 'автобус'
    },
    {
        'riddle' : 'свет',
        'answer' : 'электричество'
    }    
  ];

const ruTranslates = {
    YOU_WIN: 'Вы выиграли!',
    YOU_LOSE: 'Вы проиграли!',
    START_GAME: 'Начать игру',
    NEW_GAME: 'Новая игра',
    FOR_CHOICE_YOU_HAVE: 'Для выбора буквы у вас есть',
    SEC: 'сек',
    PROMPT: 'Подсказка: ',
    GUESS_THE_WORD: 'Отгадай слово!',
    EN: 'EN',
    RU: 'РУС',
    UK: 'УКР',
    TITLE: 'Виселица',
    DONATE: 'Поддержать игру',
}

const ukTranslates = {
    YOU_WIN: 'Ви виграли!',
    YOU_LOSE: 'Ви програли!',
    START_GAME: 'Почати гру',
    NEW_GAME: 'Нова гра',
    FOR_CHOICE_YOU_HAVE: 'Для вибору літери у вас є',
    SEC: 'сек',
    PROMPT: 'Підказка: ',
    GUESS_THE_WORD: 'Відгадай слово!',
    EN: 'EN',
    RU: 'РУС',
    UK: 'УКР',
    TITLE: 'Шибениця',
    DONATE: 'Підтримати гру',
}

const enTranslates = {
    YOU_WIN: 'You win!',
    YOU_LOSE: 'You lose!',
    START_GAME: 'Start game',
    NEW_GAME: 'New game',
    FOR_CHOICE_YOU_HAVE: 'To select a letter you have',
    SEC: 'sec',
    PROMPT: 'Prompt: ',
    GUESS_THE_WORD: 'Guess the word!',
    EN: 'EN',
    RU: 'RU',
    UK: 'UK',
    TITLE: 'Gallow',
    DONATE: 'Donate',
}

export const getLetters = (lang?: LangType): ResponseLettersType => {
    switch(lang) {
        case LangType.RU:
            return {
                letters: ruLetters,
                keyboardView: ruLettersKeyboard,
            };
        case LangType.UK:
            return {
                letters: ukLetters,
                keyboardView: ukLettersKeyboard,
            };
        case LangType.EN:
            return {
                letters: enLetters,
                keyboardView: enLettersKeyboard,
            };
        default:
            return {
                letters: ruLetters,
                keyboardView: ruLettersKeyboard,
            };;
    }
}

export const getRiddles = (lang?: LangType): Riddle[] => {
    switch(lang) {
        case LangType.RU:
            return ruRiddles;
        case LangType.UK:
            return ukRiddles;
        case LangType.EN:
            return enRiddles;
        default:
            return ruRiddles;
    }
}

export const getTranslates = (lang?: LangType) => {
    switch(lang) {
        case LangType.RU:
            return ruTranslates;
        case LangType.UK:
            return ukTranslates;
        case LangType.EN:
            return enTranslates;
        default:
            return ruTranslates;
    }    
}
