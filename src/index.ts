import config from './config/config';
import mongoose from 'mongoose';
import { createServer } from './createServer';

const app = createServer();
const { port, mongo } = config;

async function dbConnect() {
    await mongoose.connect(mongo.url, { retryWrites: true, w: 'majority' });
    console.log('DB is connected');

    app.listen(port, () => {
        console.log(`Server listening on port ${port}`);
    });
}

dbConnect()
    .catch(err => console.log(err));
