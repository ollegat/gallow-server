export const getRandomCeilNumber = (max: number) => {
    return Math.round(Math.random() * max)
}
