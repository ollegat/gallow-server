import express from 'express';
import { getRoutes } from './config/routes';

export function createServer() {
    const appExpress = express();
    const app = getRoutes(appExpress);
    return app;
}
