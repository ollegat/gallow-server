import { Application, Response } from 'express';
import config, { HTTP_STATUSES } from './config';
import { getTranslates } from '../app/controllers/translates';
import { getRiddleData, checkLetter } from '../app/controllers/riddles';
import { getLetters } from  '../app/controllers/letters';

export const getRoutes = (app: Application) => {

    const { apiV1 } = config;

    app.get('/', (req, res: Response) => {
        res.sendStatus(HTTP_STATUSES.NOT_FOUND_404);
    });
    
    app.get(`${apiV1}/translates`, getTranslates);
    
    app.get(`${apiV1}/riddle/data`, getRiddleData);
    app.get(`${apiV1}/riddles/:id/checkletter/:letter`, checkLetter);
    
    app.get(`${apiV1}/letters`, getLetters);

    return app;
}
