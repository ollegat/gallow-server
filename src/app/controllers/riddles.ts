import { Response } from 'express';
import { Types } from 'mongoose';
import { getRandomCeilNumber } from '../../utils/getRandomCeilNumber';
import {
    LangType,
    RiddleData,
    RequestGetType,
    RequestCheckLetterType,
    ResponseCheckLetter,
} from '../../entities';
import { HTTP_STATUSES } from '../../config/config';
import { RiddleModel } from '../models/riddles';

export const getRiddleData = async (req: RequestGetType, res:  Response<RiddleData>) => {
    const lang = req.query.lang
        ? req.query.lang
        : LangType.RU;
    try {
        const count = await RiddleModel.count({ lang }).exec();

        if (count === 0) {
            res.sendStatus(HTTP_STATUSES.NO_CONTENT_204);
            return;
        }
        const skipedCount = getRandomCeilNumber(count-1);

        const result = await RiddleModel.findOne({ lang })
            .skip(skipedCount)
            .exec();
        if (result) {
            const riddleData = {
                id: result._id.toString(),
                riddle: result.riddle,
                countLetters: result.answer.length+1,
            }
            res.json(riddleData);
        } else {
            res.sendStatus(HTTP_STATUSES.NO_CONTENT_204);
        }

    } catch (error) {
        console.log(error);
        res.sendStatus(HTTP_STATUSES.BAD_REQUEST_400);
    }
}

export const checkLetter = async (req: RequestCheckLetterType, res:  Response<ResponseCheckLetter>) => {
    const lang = req.query.lang
        ? req.query.lang
        : LangType.RU;
    const { id, letter } = req.params;

    const result: ResponseCheckLetter = {
        status: false,
        letter,
        positions: [],
    }

    try {
        const riddle = await RiddleModel.findOne({
            _id: new Types.ObjectId(id),
         }).exec();

        if (riddle === null) {
            throw Error('Riddle id is not found');
        }
        let word = riddle.answer;
        let pos = word.indexOf(letter);
        while(pos >= 0) {
            result.status = true;
            result.positions.push(pos);
            word = word.replace(letter,'_');
            pos = word.indexOf(letter);
        }
        res.json(result);
    } catch(error) {
        res.sendStatus(HTTP_STATUSES.BAD_REQUEST_400);
    }
}
