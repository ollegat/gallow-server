import { Response } from 'express';
import { getLetters as getMockLetters} from '../../mockAPI';
import {
    LangType,
    ResponseLettersType,
    RequestGetType,
} from '../../entities';

export const getLetters = (req: RequestGetType, res: Response<ResponseLettersType>) => {
    const lang = req.query.lang
        ? req.query.lang
        : LangType.RU;
    const letters = getMockLetters(lang);
    res.json(letters);
}
