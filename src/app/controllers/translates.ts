import { Response } from 'express';
import { getTranslates as getMockTranslates } from '../../mockAPI';
import {
    LangType,
    Translates,
    RequestGetType,
} from '../../entities';

export const getTranslates = (req: RequestGetType, res: Response<Translates>) => {
    const lang = req.query.lang
        ? req.query.lang
        : LangType.RU;
    const translates = getMockTranslates(lang);
    res.json(translates);
}
