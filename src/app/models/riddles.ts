import mongoose, { Schema, Document } from 'mongoose';
import { Riddle, LangType } from 'entities';

interface RiddleDB extends Riddle {
    lang: LangType;
}

interface IRiddleModel extends RiddleDB, Document {};

const riddleSchema = new Schema({
    riddle: { type: String, required: true },
    answer: { type: String, required: true },
    lang: { type: String, required: true },
});

export const RiddleModel = mongoose.model<IRiddleModel>('riddle', riddleSchema);
