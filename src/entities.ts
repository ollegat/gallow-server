import { Request } from 'express';

export interface Riddle {
    riddle: string;
    answer: string;
}

export type RiddleData = {
    id: string;
    riddle: Riddle['riddle'];
    countLetters: number;
}

export enum LangType {
    RU = 'RU',
    EN = 'EN',
    UK = 'UK',
}

export type AlphaBetViewType = string[];
export type KeyboardViewType = string[][];

export type ResponseLettersType = {
    letters: AlphaBetViewType;
    keyboardView: KeyboardViewType;
}

export type Translates = {
    [key: string]: string;
}

export type RequestGetType = Request<{},{},{},{lang?: LangType}>;

type Char = string  & { length: 1 };
export type RequestCheckLetterType = Request<{ id: string; letter: Char},{},{}, {lang?: LangType}>;

export type ResponseCheckLetter = {
    status: boolean;
    positions: number[];
    letter: string;
}
